package com.waijiaojun.tpo;

import com.waijiaojun.tpo.service.account.ShiroDbRealm;
import com.waijiaojun.tpo.service.account.WeixinOAuthAuthenticationFilter;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
@SpringBootApplication
public class TpoApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TpoApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(TpoApplication.class, args);
    }

    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilter() {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        Map<String, Filter> filters = new HashMap<String, Filter>();
        filters.put("weixinOAuthAuthentication", new WeixinOAuthAuthenticationFilter());
        shiroFilter.setFilters(filters);

        shiroFilter.setLoginUrl("/login");
        shiroFilter.setSuccessUrl("/index");
        shiroFilter.setUnauthorizedUrl("/forbidden");

//        http://192.168.15.42:8821/tpo/login/webLogin/343532
        Map<String, String> filterChainDefinitionMapping = new LinkedHashMap<>();//必须使用可排序的Map
        filterChainDefinitionMapping.put("/login/webLogin/**", "anon");
        filterChainDefinitionMapping.put("/login/oauthFailure", "anon");
        filterChainDefinitionMapping.put("/weixinOAuthLogin", "weixinOAuthAuthentication");
        filterChainDefinitionMapping.put("/weixin-pay-callback/**", "anon");
        filterChainDefinitionMapping.put("/logout", "logout");
        filterChainDefinitionMapping.put("/wxserver/**", "anon");
        filterChainDefinitionMapping.put("/images/**", "anon");
        filterChainDefinitionMapping.put("/css/**", "anon");
        filterChainDefinitionMapping.put("/index.html", "anon");
        filterChainDefinitionMapping.put("/**", "user");
        shiroFilter.setFilterChainDefinitionMap(filterChainDefinitionMapping);
        shiroFilter.setSecurityManager(securityManager());

        return shiroFilter;
    }

    @Bean(name = "securityManager")
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(realm());
        return securityManager;
    }

    @Bean(name = "realm")
    @DependsOn("lifecycleBeanPostProcessor")
    public Realm realm() {
        return new ShiroDbRealm();
    }

    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }
}
