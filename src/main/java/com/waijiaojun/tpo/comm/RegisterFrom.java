package com.waijiaojun.tpo.comm;

/**
 * 注册账号方式：scan：扫码关注，direct：直接关注，oauth：OAuth页面授权注册账号
 * 
 * @author Peter
 *
 */
public class RegisterFrom {

	/**
	 * scan：通过扫码关注 注册账号
	 */
	public static final String scan = "scan";
	/**
	 * direct：通过直接关注微信公众号（普通订阅） 注册账号
	 */
	public static final String direct = "direct";
	/**
	 * oauth：通过OAuth页面授权注册账号
	 */
	public static final String oauth = "oauth";

}
