package com.waijiaojun.tpo.comm;

public class ResultCode {
	
	/**
	 * code 操作成功 
	 */
	public static final String SUCCESS = "000000";
	
	/**
	 * message 操作成功 
	 */
	public static final String SUCCESS_MSG = "操作成功";
	
	
	/**
	 * code 系统繁忙 
	 */
	public static final String ERROR = "000001";
	
	/**
	 * message 系统繁忙 
	 */
	public static final String ERROR_MSG = "系统繁忙";
	
	
	/**
	 * code 用户session丢失，无法获取用户登录信息，需要用户重新登录系统
	 */
	public static final String NO_SESSION = "000002";
	
	/**
	 * message 用户session丢失，无法获取用户登录信息，需要用户重新登录系统 
	 */
	public static final String NO_SESSION_MSG = "无法获取用户信息，请重新登录";

}
