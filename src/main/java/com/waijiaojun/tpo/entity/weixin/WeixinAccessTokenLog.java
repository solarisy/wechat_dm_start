package com.waijiaojun.tpo.entity.weixin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinAccessTokenLog {

	public WeixinAccessTokenLog() {
	}
	
	private Long id; //
	private String accessToken; //获取到的凭证
	private Long expiresIn; //凭证有效时间，单位：秒
	private Date createDate; //
	private String errcode; //
	private String errmsg; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *获取到的凭证
     **/
	public String getAccessToken(){
		return accessToken;
	}
	
	/**
	 *获取到的凭证
	 **/
	public void setAccessToken(String accessToken){
		this.accessToken=accessToken;
	}
    /**
     *凭证有效时间，单位：秒
     **/
	public Long getExpiresIn(){
		return expiresIn;
	}
	
	/**
	 *凭证有效时间，单位：秒
	 **/
	public void setExpiresIn(Long expiresIn){
		this.expiresIn=expiresIn;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public String getErrcode(){
		return errcode;
	}
	
	/**
	 *
	 **/
	public void setErrcode(String errcode){
		this.errcode=errcode;
	}
    /**
     *
     **/
	public String getErrmsg(){
		return errmsg;
	}
	
	/**
	 *
	 **/
	public void setErrmsg(String errmsg){
		this.errmsg=errmsg;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}