package com.waijiaojun.tpo.entity.weixin;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinEventPush {
	
	/**
	 * click
	 */
	public static final String event_type_click = "click";
	
	/**
	 * text_directives(直接回复文本事件)
	 */
	public static final String event_type_text_directives = "text_directives";
	
	/**
	 * subscribe
	 */
	public static final String event_type_subscribe = "subscribe";

	public WeixinEventPush() {
	}
	
	private Long id; //
	private Date createDate; //
	private Date updateDate; //
	private String eventType; //
	private String eventCode; //
	private String eventName; //
	private String eventKey; //
	private String eventDesc; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *
     **/
	public String getEventType(){
		return eventType;
	}
	
	/**
	 *
	 **/
	public void setEventType(String eventType){
		this.eventType=eventType;
	}
    /**
     *
     **/
	public String getEventCode(){
		return eventCode;
	}
	
	/**
	 *
	 **/
	public void setEventCode(String eventCode){
		this.eventCode=eventCode;
	}
    /**
     *
     **/
	public String getEventName(){
		return eventName;
	}
	
	/**
	 *
	 **/
	public void setEventName(String eventName){
		this.eventName=eventName;
	}
    /**
     *
     **/
	public String getEventKey(){
		return eventKey;
	}
	
	/**
	 *
	 **/
	public void setEventKey(String eventKey){
		this.eventKey=eventKey;
	}
    /**
     *
     **/
	public String getEventDesc(){
		return eventDesc;
	}
	
	/**
	 *
	 **/
	public void setEventDesc(String eventDesc){
		this.eventDesc=eventDesc;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}