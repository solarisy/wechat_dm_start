package com.waijiaojun.tpo.entity.weixin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinPublic {

	public WeixinPublic() {
	}
	
	private Long id; //
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private String createUserUid; //创建人uid
	private String updateUserUid; //最后更新人uid
	private Integer deleted; //是否删除（0：删，1：用）
	private String appId; //
	private String appSecret; //
	private String serverUrl; //
	private String token; //
	private String encodingAESKey; //
	private String redirectUil; //
	private String publicMarkName; //
	private String publicMarkPassword; //
	private String marketingName; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *创建人uid
     **/
	public String getCreateUserUid(){
		return createUserUid;
	}
	
	/**
	 *创建人uid
	 **/
	public void setCreateUserUid(String createUserUid){
		this.createUserUid=createUserUid;
	}
    /**
     *最后更新人uid
     **/
	public String getUpdateUserUid(){
		return updateUserUid;
	}
	
	/**
	 *最后更新人uid
	 **/
	public void setUpdateUserUid(String updateUserUid){
		this.updateUserUid=updateUserUid;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
    /**
     *
     **/
	public String getAppId(){
		return appId;
	}
	
	/**
	 *
	 **/
	public void setAppId(String appId){
		this.appId=appId;
	}
    /**
     *
     **/
	public String getAppSecret(){
		return appSecret;
	}
	
	/**
	 *
	 **/
	public void setAppSecret(String appSecret){
		this.appSecret=appSecret;
	}
    /**
     *
     **/
	public String getServerUrl(){
		return serverUrl;
	}
	
	/**
	 *
	 **/
	public void setServerUrl(String serverUrl){
		this.serverUrl=serverUrl;
	}
    /**
     *
     **/
	public String getToken(){
		return token;
	}
	
	/**
	 *
	 **/
	public void setToken(String token){
		this.token=token;
	}
   
    public String getEncodingAESKey() {
		return encodingAESKey;
	}

	public void setEncodingAESKey(String encodingAESKey) {
		this.encodingAESKey = encodingAESKey;
	}

	/**
     *
     **/
	public String getRedirectUil(){
		return redirectUil;
	}
	
	/**
	 *
	 **/
	public void setRedirectUil(String redirectUil){
		this.redirectUil=redirectUil;
	}
    /**
     *
     **/
	public String getPublicMarkName(){
		return publicMarkName;
	}
	
	/**
	 *
	 **/
	public void setPublicMarkName(String publicMarkName){
		this.publicMarkName=publicMarkName;
	}
    /**
     *
     **/
	public String getPublicMarkPassword(){
		return publicMarkPassword;
	}
	
	/**
	 *
	 **/
	public void setPublicMarkPassword(String publicMarkPassword){
		this.publicMarkPassword=publicMarkPassword;
	}
    /**
     *
     **/
	public String getMarketingName(){
		return marketingName;
	}
	
	/**
	 *
	 **/
	public void setMarketingName(String marketingName){
		this.marketingName=marketingName;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}