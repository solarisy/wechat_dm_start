package com.waijiaojun.tpo.entity.weixin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinSubscribeLog {

	public WeixinSubscribeLog() {
	}
	
	private Long id; //
	private Date createDate; //
	private String openid; //
	private String event; //事件类型，subscribe(订阅)、unsubscribe(取消订阅)
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
    /**
     *事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     **/
	public String getEvent(){
		return event;
	}
	
	/**
	 *事件类型，subscribe(订阅)、unsubscribe(取消订阅)
	 **/
	public void setEvent(String event){
		this.event=event;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}