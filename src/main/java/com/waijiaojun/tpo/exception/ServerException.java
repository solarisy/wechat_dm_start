/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.waijiaojun.tpo.exception;

import org.springframework.http.HttpStatus;

/**
 * 专用于服务端的异常.
 * 
 * @author peter
 */
public class ServerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

	public ServerException() {
	}

	public ServerException(HttpStatus status) {
		this.status = status;
	}

	public ServerException(String message) {
		super(message);
	}

	public ServerException(HttpStatus status, String message) {
		super(message);
		this.status = status;
	}
}
