package com.waijiaojun.tpo.repository.weixin;

import com.waijiaojun.tpo.entity.weixin.WeixinAccessToken;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@Mapper
public interface WeixinAccessTokenDao {
	
	WeixinAccessToken getById(Long id);
	
	List<WeixinAccessToken> getAll();
	
	/**
	 * 分页查询
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinAccessToken> searchPage(@Param("weixinAccessToken") WeixinAccessToken weixinAccessToken, @Param("pageStart") int pageStart, @Param("pageSize") int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @return
	 */
	Long searchCount(WeixinAccessToken weixinAccessToken);
	
	void save(WeixinAccessToken weixinAccessToken);
	
	void update(WeixinAccessToken weixinAccessToken);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	WeixinAccessToken getByWeixinPublicId(Long publicId);
	

}
