package com.waijiaojun.tpo.repository.weixin;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.waijiaojun.tpo.entity.weixin.WeixinJsapiTicketLog;
import com.waijiaojun.tpo.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@Mapper
public interface WeixinJsapiTicketLogDao {
	
	WeixinJsapiTicketLog getById(Long id);
	
	List<WeixinJsapiTicketLog> getAll();
	
	/**
	 * 分页查询
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinJsapiTicketLog> searchPage(@Param("weixinJsapiTicketLog") WeixinJsapiTicketLog weixinJsapiTicketLog, @Param("pageStart") int pageStart, @Param("pageSize") int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @return
	 */
	Long searchCount(WeixinJsapiTicketLog weixinJsapiTicketLog);
	
	void save(WeixinJsapiTicketLog weixinJsapiTicketLog);
	
	void update(WeixinJsapiTicketLog weixinJsapiTicketLog);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}
