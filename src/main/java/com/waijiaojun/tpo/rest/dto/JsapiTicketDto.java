package com.waijiaojun.tpo.rest.dto;

/**
 * {"errcode":0,"errmsg":"ok","ticket":
 * "bxLdikRXVbTPdHSM05e5u3tgwh7S8MpcDcXHS8MO__V-YjhJbpE7gnR72fmm77V2b_t_LGHFWKI-i2V4DZRWsg"
 * ,"expires_in":7200}
 * 
 * @author Peter
 *
 */
public class JsapiTicketDto {

	private Long errcode;
	private String errmsg;
	private String ticket;
	private String expires_in;

	public Long getErrcode() {
		return errcode;
	}

	public void setErrcode(Long errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

}
