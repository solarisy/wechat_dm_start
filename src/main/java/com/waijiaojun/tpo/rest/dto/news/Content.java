package com.waijiaojun.tpo.rest.dto.news;

import java.util.List;

public class Content {

	private List<News_item> news_item;

	public void setNews_item(List<News_item> news_item){
		this.news_item = news_item;
	}

	public List<News_item> getNews_item(){
		return this.news_item;
	}
}
