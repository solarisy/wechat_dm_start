package com.waijiaojun.tpo.service.account;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

public class WeixinOAuthToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
	private static final long serialVersionUID = -8843958697998128839L;
	/**
	 * The code 作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
	 */
	private String code;

	private String state;

	private boolean rememberMe = false;

	private String host;
	public WeixinOAuthToken() {
	}

	public WeixinOAuthToken(final String code, final String state) {
		this(code, state, false, null);
	}

	public WeixinOAuthToken(final String code, final String state, final boolean rememberMe, final String host) {

		this.code = code;
		this.state = state;
		this.rememberMe = rememberMe;
		this.host = host;
	}


	/**
	 * Simply returns {@link #getCode() getCode()}.
	 *
	 * @return the {@link #getCode() code}.
	 * @see org.apache.shiro.authc.AuthenticationToken#getPrincipal()
	 */
	public Object getPrincipal() {
		return getCode();
	}
	
	/**
	 * Returns the {@link #getCode() password} char array.
	 *
	 * @return the {@link #getCode() password} char array.
	 * @see org.apache.shiro.authc.AuthenticationToken#getCredentials()
	 */
	public Object getCredentials() {
		return getCode();
	}

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	/**
	 * Clears out (nulls) the username, password, rememberMe, and inetAddress.
	 * The password bytes are explicitly set to <tt>0x00</tt> before nulling to
	 * eliminate the possibility of memory access at a later time.
	 */
	public void clear() {
		this.code = null;
		this.state = null;
		this.host = null;
		this.rememberMe = false;
	}

	/**
	 * Returns the String representation. It does not include the password in
	 * the resulting string for security reasons to prevent accidentially
	 * printing out a password that might be widely viewable).
	 *
	 * @return the String representation of the <tt>WeixinOauthToken</tt>,
	 *         omitting the password.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName());
		sb.append(" - ");
		sb.append(code);
		sb.append(", rememberMe=").append(rememberMe);
		if (host != null) {
			sb.append(" (").append(host).append(")");
		}
		return sb.toString();
	}

}
