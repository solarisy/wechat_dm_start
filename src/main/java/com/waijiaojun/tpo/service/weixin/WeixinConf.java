package com.waijiaojun.tpo.service.weixin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 微信公众平台配置，根据spring profile动态读取相应配置文件中的配置信息。
 * 
 * @author Peter
 *
 */
@Component
public class WeixinConf {
	// 微信ID
	@Value("${WeiXin.PublicId}")
	private String publicId;
	

	public Long getPublicId() {
		return Long.parseLong(publicId);
	}

	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}

}
