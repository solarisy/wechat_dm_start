package com.waijiaojun.tpo.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.waijiaojun.tpo.comm.MyPage;
import com.waijiaojun.tpo.entity.weixin.WeixinEventPushArticle;
import com.waijiaojun.tpo.repository.weixin.WeixinEventPushArticleDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinEventPushArticleService {

	private static Logger logger = LoggerFactory.getLogger(WeixinEventPushArticleService.class);

	@Autowired
	private WeixinEventPushArticleDao weixinEventPushArticleDao;

	public WeixinEventPushArticle getById(Long id) {
		return weixinEventPushArticleDao.getById(id);
	}

	public List<WeixinEventPushArticle> getAll() {
		return weixinEventPushArticleDao.getAll();
	}

	/**
	 * 分页查询
	 * @return
	 */
	public MyPage<WeixinEventPushArticle> searchPage(WeixinEventPushArticle weixinEventPushArticle, int currentPage, int pageSize) {
		MyPage<WeixinEventPushArticle> myPage = new MyPage<WeixinEventPushArticle>();

		Long count = weixinEventPushArticleDao.searchCount(weixinEventPushArticle);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinEventPushArticle> list = weixinEventPushArticleDao.searchPage(weixinEventPushArticle, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinEventPushArticle weixinEventPushArticle) {
		weixinEventPushArticleDao.save(weixinEventPushArticle);
	}

	public void update(WeixinEventPushArticle weixinEventPushArticle) {
		weixinEventPushArticleDao.update(weixinEventPushArticle);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinEventPushArticleDao.delete(id);
	}
}
