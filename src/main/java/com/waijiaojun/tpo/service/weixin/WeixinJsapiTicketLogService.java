package com.waijiaojun.tpo.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.waijiaojun.tpo.comm.MyPage;
import com.waijiaojun.tpo.entity.weixin.WeixinJsapiTicketLog;
import com.waijiaojun.tpo.repository.weixin.WeixinJsapiTicketLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinJsapiTicketLogService {

	private static Logger logger = LoggerFactory.getLogger(WeixinJsapiTicketLogService.class);

	@Autowired
	private WeixinJsapiTicketLogDao weixinJsapiTicketLogDao;

	public WeixinJsapiTicketLog getById(Long id) {
		return weixinJsapiTicketLogDao.getById(id);
	}

	public List<WeixinJsapiTicketLog> getAll() {
		return weixinJsapiTicketLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinJsapiTicketLog> searchPage(WeixinJsapiTicketLog weixinJsapiTicketLog, int currentPage, int pageSize) {
		MyPage<WeixinJsapiTicketLog> myPage = new MyPage<WeixinJsapiTicketLog>();

		Long count = weixinJsapiTicketLogDao.searchCount(weixinJsapiTicketLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinJsapiTicketLog> list = weixinJsapiTicketLogDao.searchPage(weixinJsapiTicketLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinJsapiTicketLog weixinJsapiTicketLog) {
		weixinJsapiTicketLogDao.save(weixinJsapiTicketLog);
	}

	public void update(WeixinJsapiTicketLog weixinJsapiTicketLog) {
		weixinJsapiTicketLogDao.update(weixinJsapiTicketLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinJsapiTicketLogDao.delete(id);
	}
}
