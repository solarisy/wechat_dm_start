package com.waijiaojun.tpo.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.waijiaojun.tpo.comm.MyPage;
import com.waijiaojun.tpo.entity.weixin.WeixinJsapiTicket;
import com.waijiaojun.tpo.repository.weixin.WeixinJsapiTicketDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinJsapiTicketService {

	private static Logger logger = LoggerFactory.getLogger(WeixinJsapiTicketService.class);

	@Autowired
	private WeixinJsapiTicketDao weixinJsapiTicketDao;

	public WeixinJsapiTicket getById(Long id) {
		return weixinJsapiTicketDao.getById(id);
	}

	public List<WeixinJsapiTicket> getAll() {
		return weixinJsapiTicketDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinJsapiTicket> searchPage(WeixinJsapiTicket weixinJsapiTicket, int currentPage, int pageSize) {
		MyPage<WeixinJsapiTicket> myPage = new MyPage<WeixinJsapiTicket>();

		Long count = weixinJsapiTicketDao.searchCount(weixinJsapiTicket);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinJsapiTicket> list = weixinJsapiTicketDao.searchPage(weixinJsapiTicket, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinJsapiTicket weixinJsapiTicket) {
		weixinJsapiTicketDao.save(weixinJsapiTicket);
	}

	public void update(WeixinJsapiTicket weixinJsapiTicket) {
		weixinJsapiTicketDao.update(weixinJsapiTicket);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinJsapiTicketDao.delete(id);
	}
}
