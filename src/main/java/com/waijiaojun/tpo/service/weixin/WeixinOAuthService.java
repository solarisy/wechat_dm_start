package com.waijiaojun.tpo.service.weixin;

import com.waijiaojun.tpo.utils.JsonUtil;
import com.waijiaojun.tpo.exception.ServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.waijiaojun.tpo.entity.weixin.WeixinPublic;
import com.waijiaojun.tpo.rest.dto.ReceiveAccessTokenDto;
import com.waijiaojun.tpo.rest.dto.WeixinUserInfoDto;
import com.waijiaojun.tpo.utils.HttpUtil;

import java.io.IOException;

/**
 * 微信OAuth2.0授权、认证Service
 * @author Peter
 *
 */
@Component
public class WeixinOAuthService {

	private static Logger logger = LoggerFactory.getLogger(WeixinOAuthService.class);
	
	@Autowired
	private WeixinConf weixinConf;
	
	@Autowired
	private WeixinPublicService weixinPublicService;

	
	public static final String _SS="%3A%2F%2F";//  ://
	public static final String _S="%2F";       //   /
	
	public final static void main(String[] args) {
//		recieveCode();
		//receiveAccessToken("123");
	}
	
	/**
	 * 第二步 获取网页授权access_token(注意：此access_token与基础支持的access_token不同)
	 * //https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
	 * @param code
	 * @return
	 */
	public ReceiveAccessTokenDto receiveAccessToken(String code){
		WeixinPublic weixinPublic = weixinPublicService.getGlobleWeixinPublic();
		
		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+weixinPublic.getAppId()+"&secret="+weixinPublic.getAppSecret()+"&code="+code+"&grant_type=authorization_code";
		
		String body = HttpUtil.httpGet(url);
		logger.info("----------------------------------------/n--- 第二步 获取access_token {code:"+code+"} result:-->>"+body);
		ReceiveAccessTokenDto bean = null;
		try {
			bean = JsonUtil.toObject(body, ReceiveAccessTokenDto.class);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}

		return bean;
	}
	
	//	第四步 拉取用户信息(需scope为 snsapi_userinfo)
	public WeixinUserInfoDto receiveWeixinUserInfo(String access_token,String openid){
		String url = "https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+openid+"&lang=zh_CN";
		
		String body = HttpUtil.httpGet(url);
		logger.info("----------------------------------------/n--- 第四步 拉取用户信息  result:-->>"+body);
		WeixinUserInfoDto bean = null;
		try {
			bean = JsonUtil.toObject(body, WeixinUserInfoDto.class);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw  new ServerException("读取用户信息错误,格式错误："+e.getMessage());
		}

		return bean;
	}
	
	
	//检验授权凭证（access_token）是否有效
	public boolean validateAccessToken(String accessToken,String openId){
		boolean valid = false;
		String url = "https://api.weixin.qq.com/sns/auth?access_token="+accessToken+"&openid="+openId;
		String body = HttpUtil.httpGet(url);
		System.out.println(body);
		ReceiveAccessTokenDto bean = null;
		try {
			bean = JsonUtil.toObject(body, ReceiveAccessTokenDto.class);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw  new ServerException("检验授权凭证错误,格式错误："+e.getMessage());
		}
		if("0".equals(bean.getErrcode())){
			valid = true;
		}
		
		return valid;
	}
}
