package com.waijiaojun.tpo.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.waijiaojun.tpo.comm.MyPage;
import com.waijiaojun.tpo.entity.weixin.WeixinPublic;
import com.waijiaojun.tpo.repository.weixin.WeixinPublicDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinPublicService {

	private static Logger logger = LoggerFactory.getLogger(WeixinPublicService.class);
	
	@Autowired
	private WeixinConf weixinConf;
	
	private static WeixinPublic weixinPublic;
	
	@Autowired
	private WeixinPublicDao weixinPublicDao;
	
	/**
	 * 获取微信公众号配置信息
	 * @return
	 */
	public WeixinPublic getGlobleWeixinPublic(){
		if(weixinPublic==null){
			weixinPublic = weixinPublicDao.getById(weixinConf.getPublicId());
		}
		
		return weixinPublic;
	}

	public WeixinPublic getById(Long id) {
		return weixinPublicDao.getById(id);
	}

	public List<WeixinPublic> getAll() {
		return weixinPublicDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinPublic> searchPage(WeixinPublic weixinPublic, int currentPage, int pageSize) {
		MyPage<WeixinPublic> myPage = new MyPage<WeixinPublic>();

		Long count = weixinPublicDao.searchCount(weixinPublic);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinPublic> list = weixinPublicDao.searchPage(weixinPublic, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinPublic weixinPublic) {
		weixinPublicDao.save(weixinPublic);
	}

	public void update(WeixinPublic weixinPublic) {
		weixinPublicDao.update(weixinPublic);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinPublicDao.delete(id);
	}

}
