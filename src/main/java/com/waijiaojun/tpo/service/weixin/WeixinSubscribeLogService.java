package com.waijiaojun.tpo.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.waijiaojun.tpo.comm.MyPage;
import com.waijiaojun.tpo.entity.weixin.WeixinSubscribeLog;
import com.waijiaojun.tpo.repository.weixin.WeixinSubscribeLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinSubscribeLogService {

	private static Logger logger = LoggerFactory.getLogger(WeixinSubscribeLogService.class);

	@Autowired
	private WeixinSubscribeLogDao weixinSubscribeLogDao;

	public WeixinSubscribeLog getById(Long id) {
		return weixinSubscribeLogDao.getById(id);
	}

	public List<WeixinSubscribeLog> getAll() {
		return weixinSubscribeLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinSubscribeLog> searchPage(WeixinSubscribeLog weixinSubscribeLog, int currentPage, int pageSize) {
		MyPage<WeixinSubscribeLog> myPage = new MyPage<WeixinSubscribeLog>();

		Long count = weixinSubscribeLogDao.searchCount(weixinSubscribeLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinSubscribeLog> list = weixinSubscribeLogDao.searchPage(weixinSubscribeLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinSubscribeLog weixinSubscribeLog) {
		weixinSubscribeLogDao.save(weixinSubscribeLog);
	}

	public void update(WeixinSubscribeLog weixinSubscribeLog) {
		weixinSubscribeLogDao.update(weixinSubscribeLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinSubscribeLogDao.delete(id);
	}
}
