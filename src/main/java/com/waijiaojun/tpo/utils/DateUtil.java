package com.waijiaojun.tpo.utils;

import java.text.SimpleDateFormat;

public class DateUtil {
	
	/**
	 * yyyy-MM-dd hh:mm:ss
	 */
	public static SimpleDateFormat sdf_1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	/**
	 * yyyyMMddhhmmss
	 */
	public static SimpleDateFormat sdf_2=new SimpleDateFormat("yyyyMMddhhmmss");

}
