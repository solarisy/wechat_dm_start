package com.waijiaojun.tpo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waijiaojun.tpo.utils.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Micoo on 2017/5/9.
 */
public class JsonUtil {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public static String toJson(Object obj) {
        String json = "{\"toJsonError\":\"" + obj + "\"}";
        try {
            json = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(),e);
        }

        return json;
    }

    public static <T> T toObject(String resp, Class<T> c) throws IOException {
        return mapper.readValue(resp, c);
    }
}
