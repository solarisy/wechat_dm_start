package com.waijiaojun.tpo.utils.encoder;

import java.io.IOException;

public class CEFormatException extends IOException
{
	public CEFormatException(String s)
	{
		super(s);
	}
}
