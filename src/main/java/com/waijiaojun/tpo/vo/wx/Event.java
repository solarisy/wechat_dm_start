package com.waijiaojun.tpo.vo.wx;

public enum Event {
	
	/**
	 * 订阅（用户未关注时，进行关注后的事件推送，带有EventKey，qrscene_为前缀，后面为二维码的参数值）
	 */
	subscribe,
	
	/**
	 * 取消订阅
	 */
	unsubscribe,
	
	/**
	 * 扫描带参数二维码事件: 用户已关注时的事件推送
	 */
	SCAN,
	
	/**
	 * 上报地理位置事件
	 */
	LOCATION,
	
	/**
	 * 自定义菜单事件
	 */
	CLICK
}
