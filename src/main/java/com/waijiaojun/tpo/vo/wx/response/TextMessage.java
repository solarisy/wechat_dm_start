package com.waijiaojun.tpo.vo.wx.response;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.waijiaojun.tpo.vo.xml.AdapterCDATA;

@XmlRootElement(name = "xml")
public class TextMessage extends BaseMessage {

	@XmlJavaTypeAdapter(AdapterCDATA.class)
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
	
}
