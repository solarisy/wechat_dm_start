package com.waijiaojun.tpo.web;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Peter on 2018/1/20.
 */
@Controller
public class MyErrorController implements ErrorController {
    private static final String PATH = "/error";

    @RequestMapping(PATH)
    public String handle(HttpServletRequest request, Model model) {
        String errorCode = "599";
        Object errorCode_obj =  request.getAttribute("javax.servlet.error.status_code");
        if(errorCode_obj != null){
            errorCode = errorCode_obj.toString();
        }

        String errorMsg="系统错误，请联系管理员";
        Object errorMsg_obj =  request.getAttribute("javax.servlet.error.message");
        if(errorCode_obj!=null){
            errorMsg=errorMsg_obj.toString();
        }
        model.addAttribute("errorCode", errorCode);
        model.addAttribute("errorMsg", errorMsg);

        return "error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
