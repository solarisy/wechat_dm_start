/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50635
Source Host           : 192.168.15.42:3306
Source Database       : tpo

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-01-22 14:43:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wx_weixin_access_token
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_access_token`;
CREATE TABLE `wx_weixin_access_token` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `access_token` varchar(50) NOT NULL DEFAULT '' COMMENT 'accessToken',
  `expires_in` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'expiresIn',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_access_token`';

-- ----------------------------
-- Records of wx_weixin_access_token
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_access_token_log
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_access_token_log`;
CREATE TABLE `wx_weixin_access_token_log` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `access_token` varchar(50) NOT NULL DEFAULT '' COMMENT 'accessToken',
  `expires_in` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'expiresIn',
  `errcode` varchar(50) NOT NULL DEFAULT '' COMMENT 'errcode',
  `errmsg` varchar(50) NOT NULL DEFAULT '' COMMENT 'errmsg',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_access_token_log`';

-- ----------------------------
-- Records of wx_weixin_access_token_log
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_event_push
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_event_push`;
CREATE TABLE `wx_weixin_event_push` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `event_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'eventType',
  `event_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'eventCode',
  `event_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'eventName',
  `event_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'eventKey',
  `event_desc` varchar(50) NOT NULL DEFAULT '' COMMENT 'eventDesc',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_event_push`';

-- ----------------------------
-- Records of wx_weixin_event_push
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_event_push_article
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_event_push_article`;
CREATE TABLE `wx_weixin_event_push_article` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `push_event_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'pushEventCode',
  `description` varchar(50) NOT NULL DEFAULT '' COMMENT 'description',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT 'title',
  `pic_url` varchar(50) NOT NULL DEFAULT '' COMMENT 'picUrl',
  `url` varchar(50) NOT NULL DEFAULT '' COMMENT 'url',
  `seq` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'seq',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_event_push_article`';

-- ----------------------------
-- Records of wx_weixin_event_push_article
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_h5pay_notify
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_h5pay_notify`;
CREATE TABLE `wx_weixin_h5pay_notify` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `return_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'returnCode',
  `return_msg` varchar(50) NOT NULL DEFAULT '' COMMENT 'returnMsg',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'mchId',
  `device_info` varchar(50) NOT NULL DEFAULT '' COMMENT 'deviceInfo',
  `nonce_str` varchar(50) NOT NULL DEFAULT '' COMMENT 'nonceStr',
  `sign` varchar(50) NOT NULL DEFAULT '' COMMENT 'sign',
  `result_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'resultCode',
  `err_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'errCode',
  `err_code_des` varchar(50) NOT NULL DEFAULT '' COMMENT 'errCodeDes',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid',
  `is_subscribe` varchar(50) NOT NULL DEFAULT '' COMMENT 'isSubscribe',
  `trade_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'tradeType',
  `bank_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'bankType',
  `total_fee` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'totalFee',
  `fee_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'feeType',
  `cash_fee` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'cashFee',
  `cash_fee_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'cashFeeType',
  `coupon_fee` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'couponFee',
  `coupon_count` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'couponCount',
  `coupon_id0` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'couponId0',
  `coupon_fee0` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'couponFee0',
  `transaction_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'transactionId',
  `out_trade_no` varchar(50) NOT NULL DEFAULT '' COMMENT 'outTradeNo',
  `attach` varchar(50) NOT NULL DEFAULT '' COMMENT 'attach',
  `time_end` varchar(50) NOT NULL DEFAULT '' COMMENT 'timeEnd',
  `my_sign` varchar(50) NOT NULL DEFAULT '' COMMENT 'mySign',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_h5pay_notify`';

-- ----------------------------
-- Records of wx_weixin_h5pay_notify
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_jsapi_ticket
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_jsapi_ticket`;
CREATE TABLE `wx_weixin_jsapi_ticket` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `jsapi_ticket` varchar(50) NOT NULL DEFAULT '' COMMENT 'jsapiTicket',
  `expires_in` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'expiresIn',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_jsapi_ticket`';

-- ----------------------------
-- Records of wx_weixin_jsapi_ticket
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_jsapi_ticket_log
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_jsapi_ticket_log`;
CREATE TABLE `wx_weixin_jsapi_ticket_log` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `jsapi_ticket` varchar(50) NOT NULL DEFAULT '' COMMENT 'jsapiTicket',
  `expires_in` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'expiresIn',
  `errcode` varchar(50) NOT NULL DEFAULT '' COMMENT 'errcode',
  `errmsg` varchar(50) NOT NULL DEFAULT '' COMMENT 'errmsg',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_jsapi_ticket_log`';

-- ----------------------------
-- Records of wx_weixin_jsapi_ticket_log
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_public
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_public`;
CREATE TABLE `wx_weixin_public` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_user_uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'createUserUid',
  `update_user_uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'updateUserUid',
  `deleted` int(11) NOT NULL DEFAULT '0' COMMENT 'deleted',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'appId',
  `app_secret` varchar(50) NOT NULL DEFAULT '' COMMENT 'appSecret',
  `server_url` varchar(50) NOT NULL DEFAULT '' COMMENT 'serverUrl',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'token',
  `encoding_a_e_s_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'encodingAESKey',
  `redirect_uil` varchar(50) NOT NULL DEFAULT '' COMMENT 'redirectUil',
  `public_mark_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'publicMarkName',
  `public_mark_password` varchar(50) NOT NULL DEFAULT '' COMMENT 'publicMarkPassword',
  `marketing_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'marketingName',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'mchId',
  `pay_notify_url` varchar(50) NOT NULL DEFAULT '' COMMENT 'payNotifyUrl',
  `pay_api_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'payApiKey',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_public`';

-- ----------------------------
-- Records of wx_weixin_public
-- ----------------------------
INSERT INTO `wx_weixin_public` VALUES ('1', '1', '1', '0', '123', '123', '', '123', '', '', '', '', '', '', '', '', '2018-01-22 14:04:55', '2018-01-22 14:04:55');

-- ----------------------------
-- Table structure for wx_weixin_scan_log
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_scan_log`;
CREATE TABLE `wx_weixin_scan_log` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid',
  `scene_str` varchar(50) NOT NULL DEFAULT '' COMMENT 'sceneStr',
  `parent_scene_str` varchar(50) NOT NULL DEFAULT '' COMMENT 'parentSceneStr',
  `case_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'caseName',
  `case_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'caseCode',
  `current_parent_scene_str` varchar(50) NOT NULL DEFAULT '' COMMENT 'currentParentSceneStr',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_scan_log`';

-- ----------------------------
-- Records of wx_weixin_scan_log
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_subscribe_log
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_subscribe_log`;
CREATE TABLE `wx_weixin_subscribe_log` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid',
  `event` varchar(50) NOT NULL DEFAULT '' COMMENT 'event',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_subscribe_log`';

-- ----------------------------
-- Records of wx_weixin_subscribe_log
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_two_dimensional_code
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_two_dimensional_code`;
CREATE TABLE `wx_weixin_two_dimensional_code` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `image_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'imageName',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid',
  `scene_str` varchar(50) NOT NULL DEFAULT '' COMMENT 'sceneStr',
  `image_url` varchar(50) NOT NULL DEFAULT '' COMMENT 'imageUrl',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_two_dimensional_code`';

-- ----------------------------
-- Records of wx_weixin_two_dimensional_code
-- ----------------------------

-- ----------------------------
-- Table structure for wx_weixin_user_info
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_user_info`;
CREATE TABLE `wx_weixin_user_info` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid',
  `nickname` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT 'nickname',
  `sex` varchar(50) NOT NULL DEFAULT '' COMMENT 'sex',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT 'province',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT 'city',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT 'country',
  `headimgurl` varchar(50) NOT NULL DEFAULT '' COMMENT 'headimgurl',
  `privilege` varchar(50) NOT NULL DEFAULT '' COMMENT 'privilege',
  `unionid` varchar(50) NOT NULL DEFAULT '' COMMENT 'unionid',
  `weixin_public_id` bigint(15) NOT NULL DEFAULT '-1' COMMENT 'weixinPublicId',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='`wx_weixin_user_info`';

-- ----------------------------
-- Records of wx_weixin_user_info
-- ----------------------------
INSERT INTO `wx_weixin_user_info` VALUES ('1', '123', 'peter', '1', 'a', 'b', 'c', '', '', '', '1', '2018-01-22 13:31:31', '2018-01-22 14:04:01');
